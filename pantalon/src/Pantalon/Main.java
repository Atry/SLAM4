package Pantalon;

import java.util.Scanner;

public class Main {
	
	/*
	 * programme qui permettra de calculer le nombre de points acquis par un athlète :
	 * 	- après chaque discipline (escrime, natation, équitation), le nombre de points de l'épreuve,
	 * 	- après chaque discipline (natation, équitation), le nombre de points acquis au total.
	 * Bien sûr, s'il y a abandon ou disqualification en cours d'épreuve, inutile de calculer la suite !
	 */
	

	// déclaration des variables globales
	static int[] points = new int[4];
	static Boolean abandon = false;
	static Boolean disqualifie = false;
	static String athlète;
	static int rangArrivee;
	
	static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		/* test pour ne rien saisir !!!!!
		Scanner in = new Scanner(System.in);
        String resultat;
        do{
            resultat = in.nextLine();
            if("".equals(resultat))
                System.out.println("Lancer l'action voulu !");
            else
                System.out.println("Lancer une autre action ou ne rien faire");
        }while(!"quitter".equals(resultat));
        System.out.println("Fin de programme.");*/
        
		System.out.print("   Bienvenue aux JO des STS2 - dans la discipline Pentathlon \n "
				+ "Saisir prénom et nom de votre athlète ? : ");
		athlète = scanner.next()+" "+scanner.next();
		//String s = scanner.next();
		
		//1- gestion de la discipline escrime
		affichageUn("D ' E S C R I M E");
		gestionEscrime();
		affichageDeux("D ' E S C R I M E",0);
		
		if (disqualifie == false && abandon == false) {
			//2- gestion de la discipline natation
			affichageUn("D E   N A T A T I O N");
			gestionNatation();
			affichageDeux("D E   N A T A T I O N",1);
			
			if (disqualifie == false && abandon == false) {
				//3- gestion de la discipline équitation
				affichageUn("D ' E Q U I T A T I O N");
				gestionEquitation();
				affichageDeux("D ' E Q U I T A T I O N",2);
				
				if (disqualifie == false && abandon == false) {
					//4- gestion de la discipline course/tir
					affichageUn("D E   C O U R S E / T I R");
					gestionCourseTir();
				}					
			}
		}
		
	}
	
	/********* les fonctions utilisées ***********/
	
	// fonction d'affichage simple
	public static void affichageUn(String discipline)  {
		System.out.println("\n\t*************************************************"
				+ "\n\t     E P R E U V E  "+discipline+
				"\n\t*************************************************\n");
	}
	
	//fonction affichage résultat par discipline
	public static void affichageDeux(String discipline, int epreuve) {
		if (abandon==true) {
			System.out.println(athlète + " déclare forfait !!! ");
		}
		else if (disqualifie==true) {
				System.out.println(athlète + " est tout simplement disqualifié !!!");
			 }
			 else {
				 int x=0;
				 for (int i=0; i<=epreuve;i++){
					 x+=points[i];
					// System.out.println("points "+i+ " -> "+points[i]+"et x vaut : "+x);
				 }
			    System.out.println("Après l'épreuve "+discipline+" - "+athlète +" - a à son compteur "+x+" points");
			 }
	}
	
	/**
	 * Effectue la saisie d'un nombre entier
	 * @return L'entier saisi [int]
	 */
	public static int saisirUnEntier() throws Exception
	{
		int nombreSaisi = 0;
		try {
			nombreSaisi = scanner.nextInt();
		} catch (Exception ex) {
			//On vide le scanner
			String s = scanner.next();
						
			//On relève une exception pour avoir une erreur plus parlante
			throw new Exception("\n/!\\ Veuillez saisir un nombre entier !");
		}
		
		return nombreSaisi;
	}//fin
	
	public static void gestionEscrime() {
		/* Pour notre calcul, on demandera quel est le pourcentage d'assauts gagnés par l'athlète :
		 * → un pourcentage compris entre 10 % et 25 % donnera à l'athlète 100 points,
		 * → un pourcentage compris entre 26 % et 50 % donnera à l'athlète 200 points,
	 	 * → un pourcentage compris entre 51 % et 65 % donnera à l'athlète 230 points,
		 * → au-delà, ce sera 250 points.
		 * L'athlète peut cependant être disqualifié si le pourcentage est en dessous de 10 %. 
		 * */
		Boolean ok = false;
		System.out.print(athlète+" a-t-il abandonné pendant cette épreuve ? (o / n) : ");
		String s = scanner.next();
		
		if (s.toLowerCase().equals("o")) {
			abandon=true;
			points[0] = 0;
		}
		else {
			int pourcentage=0;
			System.out.print("Quel est le pourcentage de votre athlète - "+athlète +" ?\n (saisir valeur entière) : ");
			do {	
			try {
				pourcentage = saisirUnEntier();	//On effectue une saisie
				ok=true;
			} catch (Exception ex) {
				//Si une exception a été levée, on affiche le message d'erreur, et on réeffectue la saisie.
				System.err.println(ex.getMessage()+ " ressaisir le pourcentage.");
			}
			} while(ok==false);
			
			if (pourcentage < 10) {
				disqualifie = true;
				points[0]=0;
				 
			}
			else
				if (pourcentage <= 25) {
					points[0]=100;
					 
				}
				else
					if (pourcentage <= 50) {
						points[0] =200;
					}
					else 
						if (pourcentage <= 65) {
							points[0]= 230;
						}
						else // n'est pas géré ici le fait qu'on saisisse une valeur > à 100 ... TODO
						{ points[0]= 250; }
			}	
		System.out.println("Pour cette épreuve "+athlète+" a rapporté "+points[0]+" points.");
		
	}
	
	public static void gestionNatation() {
		/* → Considérons donc que, pour un temps de 2 min 30 s, le compétiteur obtient 250 points. 
		 * Chaque seconde en + ou - donne 1 point de - ou + respectivement sur la base de 250 points.
		 * L'athlète peut cependant être disqualifié si il met plus de 3 min 30s ou s'il se noie ! 
		 * */
		// Pour simplifier le calcul - on demande à l'utilisateur de saisir le temps en secondes
		Boolean ok=false;
		System.out.print(athlète+" a-t-il abandonné pendant cette épreuve ? (o / n) : ");
		String s = scanner.next();
		
		if (s.equals("o")) {
			abandon=true;
			points[1]=0;
		}
		else {
			System.out.print("Quel est le temps mis par votre athlète - "+athlète+" ? (temps en sdes) : ");
			int temps=0;
			do {
			try {
				temps = saisirUnEntier();	//On effectue une saisie
				ok=true;
			} catch (Exception ex) {
				//Si une exception a été levée, on affiche le message d'erreur, et on réeffectue la saisie.
				System.err.println(ex.getMessage()+" ressaisir le temps");
			}	
			}while (ok==false);
		
			if (temps <= 150)
				points[1]= (250 + (150 - temps));
			else
				if (temps <=210)
					points[1] = (250 - (temps-150));
				else
				{
					disqualifie=true;
					points[1] = 0;
				}
		}
		System.out.println("Pour cette épreuve "+athlète+" a rapporté "+points[1]+" points sur 250.");
				
	}
	
	public static void gestionEquitation() {
		/* → Nous déterminons ici que chaque barre que le couple athlète/cheval fait tomber 
		 * « coûte » 10 points en moins par rapport à 100 points.
		 * un refus compte 20 points de pénalités.
		 * → l'athlète peut être disqualifié si il tombe ou si le cheval fait 3 refus. 
		 * */
		Boolean ok = false;
		System.out.print(athlète+" a-t-il abandonné pendant cette épreuve ? (o / n) : ");
		String s = scanner.next();
		
		if (s.equals("o")) {
			abandon=true;
			points[2]= 0;
		}
		else { // non abandon - test du nombre de refus
			System.out.print("Combien de refus ?  ");
			int refus=0;
		    do {
			try {
				refus = saisirUnEntier();	//On effectue une saisie
				ok=true;
			} catch (Exception ex) {
				//Si une exception a été levée, on affiche le message d'erreur, et on réeffectue la saisie.
				System.err.println(ex.getMessage() + "ressaisir le nombre de refus");			
			}
		    }while (ok==false);
			if (refus>=3) {
				disqualifie=true;
				points[2] = 0;
			}
			else { // si non disqualifié, on calcule les points en fonction du nombre de barres tombées			
				
				System.out.print("combien de barres le couple a t'il fait tomber ??? : ");
				int barres=0;
				ok=false;
				do {
				try {
					barres = saisirUnEntier();	//On effectue une saisie
					ok=true;
				} catch (Exception ex) {
					//Si une exception a été levée, on affiche le message d'erreur, et on réeffectue la saisie.
					System.out.println(ex.getMessage()+ "ressaisir le nombre de barres");
				}
				} while(ok==false);
		
				points[2] = (100 - ((barres*10)+(refus*20)));
				
			}
		}
		System.out.println("Pour cette épreuve "+athlète+" a rapporté "+points[2]+" points sur 100.");
		
	}
	
	public static void gestionCourseTir() {
		/* → on notera juste le rang d'arrivée du pentathlète, 
		 * ou si éventuellement il a abandonné en cours de course.
		 */
		
		System.out.print(athlète+" a-t-il abandonné pendant cette épreuve ? (o / n) : ");
		String s = scanner.next();
		
		if (s.equals("o")) {
			abandon=true;
		}
		else {
			System.out.print("A quel rang est arrivé votre athlète "+athlète+" ??? : ");
			Boolean ok=false;
			do {
				try {
					points[3] = saisirUnEntier();	//On effectue une saisie
					ok=true;
				} catch (Exception ex) {
			//Si une exception a été levée, on affiche le message d'erreur, et on réeffectue la saisie.
					System.out.println(ex.getMessage()+ "ressaisir le nombre de barres");
				}
			} while(ok==false);
		}
		
		if (abandon==true) {
			System.out.println("Désolé, l'athlète "+athlète+" a abandonné !!!\n\n\tFIN DU PENTATHLON POUR "+athlète);
		}
		else {
			int nombrePoints=points[0]+points[1]+points[2];
			System.out.println("\n\n\tFIN DU PENTATHLON\n\t"+athlète+" termine "+points[3]+"ème avec "+nombrePoints+" points");
		}
		
	}
}